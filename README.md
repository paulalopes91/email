# README #
Este documento possui as instruções para rodar a API email criada para o desafio proposto. 

### What is this repository for? ###

O sistema fornece ao usuário um gerenciador de Emails. O pacote contém um front-end que o permite inserir suas credenciais, consultar e-mails em sua caixa de entrada, além de atualizar, deletar e salvar mensagens. Este gerenciador possui autenticação JWT, tratamento de exceções, testes unitários, validações nos campos que se encontram no request, como tamanho da mensagem, validação no formato do e-mail. A base de dados escolhida foi o MySQL e existe um swagger para a especificação e documentação da API, além de testes simulados no postman. Lembrando que durante todo o desenvolvimento desta API houve uma atenção para a utilização de boas práticas e atendimento aos requisitos técnicos.


### How do I get set up? ###
Possuir os seguintes recursos instalados na máquina: Maven, IDE que permita a linguagem Java de sua preferência, jdk 1.8, MySql


### Contribution guidelines ###

- Realizar o download do pacote contido neste repositório.
- Descompactar o projeto em um diretório de sua preferência.
- Abrir a IDE escolhida, clicar em File > Import...
- Selecionar a pasta raiz do projeto, e clicar em Finish.
- Abrir um prompt de comando, ir até o diretório escolhido e executar o comando: mvn clean install.
- Após a aplicação ser buildada com sucesso, ir até a IDE, clicar com botão direito em cima da classe "EmailApplication.java"
- Selecione a opção Run as>Java Application para apenas rodar a aplicação ou Debug as>Java Application para conseguir acompanhá-la em tempo de
execução.
- É necessário ter uma base do MySql apontada para localhost/email na porta 8080, com a senha e usuário "root"
- Executar o script que encontra-se na pasta emailapi>src>main>resources>sql do projeto
- emailapi>src>main>resources>frontend>index.html. Browser utilizado para testes: Google Chrome
-O arquivo .yaml da documentação via swagger está em: emailapi>src>main>resources>documentation
- Para o arquivo contendo os testes do postman, entrar em emailapi>src>test>postman_scripts
- Testes unitários implementados na pasta emailapi\src\test