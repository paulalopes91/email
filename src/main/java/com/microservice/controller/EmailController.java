package com.microservice.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.microservice.exception.BadRequestException;
import com.microservice.model.Email;
import com.microservice.service.EmailService;
import com.microservice.validation.EmailValidator;
import com.microservice.validation.ErrorInformation;

@Controller
public class EmailController {

	private EmailService service;

	private EmailValidator validator;

	@Autowired
	EmailController(EmailService service, EmailValidator validator) {
		this.service = service;
		this.validator = validator;
	}

	@RequestMapping(value = "/email/{id}", method = RequestMethod.GET)
	public ResponseEntity<Email> getEmail(@PathVariable Long id) {
		Email email = service.getSingleEmail(id);
		if (null != email) {
			return new ResponseEntity<>(email, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(value = "/email", method = RequestMethod.GET)
	public ResponseEntity<List<Email>> getEmailList() {
		List<Email> emailList = service.listEmail();
		return new ResponseEntity<>(emailList, HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.OPTIONS)
	public void corsHeaders(HttpServletResponse response) {
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
		response.addHeader("Access-Control-Allow-Headers", "origin, content-type, accept, x-requested-with");
		response.addHeader("Access-Control-Max-Age", "3600");
	}
	
	@RequestMapping(value = "/email", method = RequestMethod.POST)
	public ResponseEntity<Email> sendEmail(@RequestBody Email email) throws BadRequestException {
		validator.validate(email);
		email = service.postEmail(email);
		return new ResponseEntity<>(email, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/email", method = RequestMethod.PUT)
	public ResponseEntity<Void> updateEmail(@RequestBody Email email) throws BadRequestException {
		validator.validate(email);
		if (service.updateEmail(email)) {
			return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(value = "/email/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> deleteEmail(@PathVariable Long id) {
		if (service.deleteEmail(id)) {
			return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
	}

	@ExceptionHandler(BadRequestException.class)
	public ResponseEntity<ErrorInformation> badRequestException(BadRequestException e) {
		return new ResponseEntity<>(new ErrorInformation(e.getErrors()), HttpStatus.BAD_REQUEST);
	}
	
}