package com.microservice.exception;

import java.util.List;

import com.microservice.model.ValidationErrorEntity;

public class BadRequestException extends Exception {

    private String message;
    private List<ValidationErrorEntity> errors;

    public BadRequestException(String message, List<ValidationErrorEntity> errors){
        this.message = message;
        this.errors = errors;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public List<ValidationErrorEntity> getErrors() { return errors; }
}
