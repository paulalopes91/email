package com.microservice.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name="email")
@JsonInclude(Include.NON_NULL)
public class Email {
	
	@Id
	@GeneratedValue	
	@JsonProperty(value="id")
	private Long id;
	
	@Column(name="remetente")
	@JsonProperty(value="from")
	private String from;

	@Column(name="destinatario")
	@JsonProperty(value="to")
	private String to;
	
	@Column(name="mensagem")
	@JsonProperty(value="message")
	private String message;

	@Column(name="subject")
	@JsonProperty(value="subject")
	private String subject;
	
	public Email(){
		
	}
	
	public Email(Long id, String to, String from, String message){
		this.id = id;
		this.to = to;
		this.from = from;
		this.message = message;
	}
	
	public Email(String to, String from, String message){
		this.to = to;
		this.from = from;
		this.message = message;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	
}