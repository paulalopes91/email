package com.microservice.model;

public enum FieldsEnum {
    ID("id"),
    FROM("from"),
    TO("to"),
    MESSAGE("message"),
    SUBJECT("subject");

    private String field;

    FieldsEnum(String field){
        this.field = field;
    }

    public String getField() {
        return field;
    }
}
