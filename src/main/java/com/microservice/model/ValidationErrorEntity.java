package com.microservice.model;

import com.microservice.validation.RestrictionType;

public class ValidationErrorEntity {
    public String field;
    public RestrictionType type;
    public String message;

    public ValidationErrorEntity(String field, RestrictionType type, String message){
        this.field = field;
        this.type = type;
        this.message = message;
    }

    @Override
    public String toString() {
        return "ValidationErrorEntity{" +
                "field='" + field + '\'' +
                ", type=" + type +
                ", message='" + message + '\'' +
                '}';
    }
}
