package com.microservice.repository;

import org.springframework.data.repository.CrudRepository;

import com.microservice.model.Email;

public interface EmailRepository extends CrudRepository<Email, Long> {
	
}
