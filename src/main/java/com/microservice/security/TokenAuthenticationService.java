package com.microservice.security;

import java.io.IOException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.core.Authentication;




class TokenAuthenticationService {

    private String secret = "DESAFIO";
    private String headerString = "Authorization";

    void addAuthentication(HttpServletResponse response, String username) throws IOException {
        long EXPIRATIONTIME = 1000 * 60 * 60 * 24 * 10; // 10 dias
        //Geração token JWT.
        String jwt = Jwts.builder()
                .setSubject(username)
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRATIONTIME))
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();

        response.setContentType("application/json");
        response.addHeader(headerString, jwt);
        response.setStatus(202);
        response.getWriter().write("{\"" + headerString + "\": \""+ jwt + "\"}");
        response.getWriter().flush();
        response.getWriter().close();
    }

    Authentication getAuthentication(HttpServletRequest request) {
        String token = request.getHeader(headerString);
        if (token != null) {
            // parse the token.
            String username = Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(token)
                    .getBody()
                    .getSubject();
            if (username != null) // tratando para retornar um usuario
            {
                return new AuthenticatedUser(username);
            }
        }
        return null;
    }
}