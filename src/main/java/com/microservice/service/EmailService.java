package com.microservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.microservice.exception.BadRequestException;
import com.microservice.model.Email;
import com.microservice.repository.EmailRepository;


@Service
public class EmailService {
	
	private EmailRepository repository;

	    @Autowired
	    EmailService(EmailRepository repository){
	        this.repository = repository;
	    }

	    public Email getSingleEmail(Long id){
	        Email email;
	        try {
	            email = repository.findOne(id);
	        }catch (Exception e){
	            return null;
	        }
	        return email;
	    }
	    
	    public Boolean deleteEmail(Long id){
	    	try{
	    		repository.delete(id);
	        	return Boolean.TRUE;
	        }
	        catch (Exception e){
	        	return Boolean.FALSE;
	        }
	    }
	    
	    public Boolean updateEmail(Email email) throws BadRequestException {
	        try{
	        	email = repository.save(email);
	        	return Boolean.TRUE;
	        }
	        catch (Exception e){
	        	return Boolean.FALSE;
	        }
	    }
	

	    public Email postEmail(Email email) {
	        email = repository.save(email);
	        return email;
	    }

		public List<Email> listEmail() {
			List<Email> emailList;
	        try {
	            emailList = (List<Email>) repository.findAll();
	        }catch (Exception e){
	            return null;
	        }
	        return emailList;
		}
	
}
