package com.microservice.validation;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.stereotype.Service;

import com.microservice.exception.BadRequestException;
import com.microservice.model.Email;
import com.microservice.model.ValidationErrorEntity;

import static com.microservice.validation.RestrictionType.*;
import static com.microservice.validation.ErrorMessage.*;
import static com.microservice.model.FieldsEnum.*;


@Service
public class EmailValidator {

    private List<ValidationErrorEntity> errors = new ArrayList<>();
    private static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    public void validate (Email email) throws BadRequestException {
    	
    	if(errors.size()>0){
    		errors.clear();
    		}
        if(null == email.getFrom() || email.getFrom().isEmpty()){
            errors.add(new ValidationErrorEntity(FROM.getField(), REQUIRED, VALUE_NOT_SUPPLIED.getMessage()));
        }else{
            if(!validateEmail(email.getFrom())){
                errors.add(new ValidationErrorEntity(FROM.getField(), EMAIL_FORMAT, INVALID_EMAIL.getMessage()));
            }
        }

        if(null == email.getTo() || email.getTo().isEmpty()){
            errors.add(new ValidationErrorEntity(TO.getField(), REQUIRED, VALUE_NOT_SUPPLIED.getMessage()));
        }else{
            if(!validateEmail(email.getTo())){
                errors.add(new ValidationErrorEntity(TO.getField(), EMAIL_FORMAT, INVALID_EMAIL.getMessage()));
            }
        }

        if(null == email.getSubject() || email.getSubject().isEmpty()){
            errors.add(new ValidationErrorEntity(SUBJECT.getField(), REQUIRED, VALUE_NOT_SUPPLIED.getMessage()));
        }else{
            if(email.getSubject().length() > 50){
                errors.add(new ValidationErrorEntity(SUBJECT.getField(), BUSINESS, SUBJECT_LENGTH.getMessage()));
            }
        }

        if(null == email.getMessage() || email.getMessage().isEmpty()){
            errors.add(new ValidationErrorEntity(MESSAGE.getField(), REQUIRED, VALUE_NOT_SUPPLIED.getMessage()));
        }else{
            if(email.getMessage().length() > 500){
                errors.add(new ValidationErrorEntity(MESSAGE.getField(), BUSINESS, MESSAGE_LENGTH.getMessage()));
            }
        }

        if(!errors.isEmpty()){
            throw new BadRequestException(errors.toString(), errors);
        }
    }

    private Boolean validateEmail(String email) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(email);
        return matcher.find();
    }
}