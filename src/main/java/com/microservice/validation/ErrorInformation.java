package com.microservice.validation;

import java.util.List;

import com.microservice.model.ValidationErrorEntity;

public class ErrorInformation {

    private List<ValidationErrorEntity> errors;

    public ErrorInformation(List<ValidationErrorEntity> errors){
        this.errors = errors;
    }

    public List<ValidationErrorEntity> getErrors() {
        return errors;
    }

    public void setErrors(List<ValidationErrorEntity> errors) {
        this.errors = errors;
    }
}
