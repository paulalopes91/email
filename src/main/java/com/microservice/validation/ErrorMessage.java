package com.microservice.validation;


public enum ErrorMessage {
    INVALID_EMAIL("Formato de Email Inválido!"),
    VALUE_NOT_SUPPLIED("Valor não preenchido!"),
    MESSAGE_LENGTH("Mensagem maior que 500 caracteres!"),
    SUBJECT_LENGTH("Assunto maior que 50 caracteres!"),
    DIFFERENT_ID("ID enviado na URI diferente do enviado no Body");

    private String message;

    ErrorMessage(String message){
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
