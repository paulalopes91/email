package com.microservice.validation;

public enum RestrictionType {
		REQUIRED,
	    BUSINESS,
	    EMAIL_FORMAT
}
