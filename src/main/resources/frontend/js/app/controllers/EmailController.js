class EmailController {

  constructor(){
    let $ = document.querySelector.bind(document);
    this._postFrom = $("#postFrom");
    this._postTo = $("#postTo");
    this._postSubject = $("#postSubject");
    this._postMessage = $("#postMessage");
    this._putFrom = $("#putFrom");
    this._putTo = $("#putTo");
    this._putSubject = $("#putSubject");
    this._putMessage = $("#putMessage");
    this._putId = $("#putId");
    this._getId = $("#getId");
    this._deleteId = $("#deleteId");
    this._mensagem = '';
  }

  apagarEmail(event, service){
    event.preventDefault();
    //service.apagarEmail(this._id);
    service.apagarEmail((erro, sucesso) => {
      if(erro) {
        this._mensagem = erro;
        console.log(this._mensagem);
        return;
      }

      this._mensagem = sucesso;
      console.log(this._mensagem);
    }, this._deleteId.value);
  }

  obterEmail(service) {
    Promise.all([
      service.obterEmail()]
    ).then(email => {
      console.log(email);
      document.getElementById("emailList").value = JSON.stringify(email);
    }).catch(erro => {
      console.log(erro);
      this._mensagem = erro;
    });
  }

  obterEmailId(service){
    Promise.all([
      service.obterEmailId(this._getId.value)]
    ).then(email => {
      console.log(email);
      document.getElementById("emailById").value = JSON.stringify(email);
    }).catch(erro => {
      console.log(erro);
      this._mensagem = erro;
    });
  }

  enviaEmail(event, service){
    event.preventDefault();
    let email = this._emailPost();
    console.log(email);
    service.enviarEmail((erro, sucesso) => {
      if(erro) {
        this._mensagem = erro;
        console.log(this._mensagem);
        return;
      }

      this._mensagem = sucesso;
      console.log(this._mensagem);
    }, email);
  }

  atualizarEmail(event, service){
    event.preventDefault();
    let email = this._emailPut();
    console.log(email);
    service.atualizarEmail((erro, sucesso) => {
      if(erro) {
        this._mensagem = erro;
        console.log(this._mensagem);
        return;
      }

      this._mensagem = sucesso;
      console.log(this._mensagem);
    }, email);

  }

  _emailPost(){
    return new Email(this._postFrom.value, this._postTo.value, this._postSubject.value, this._postMessage.value, null);
  }

  _emailPut(){
    return new Email(this._putFrom.value, this._putTo.value, this._putSubject.value, this._putMessage.value, this._putId == null ? null : this._putId.value);
  }

  _limpaFormulario(){
    this._from;
    this._to;
    this._subject;
    this._message;
    this._from.focus();
  }
}