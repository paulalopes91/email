class Email{
  constructor(from, to, subject, message, id){
    this._id = id;
    this._from = from;
    this._to = to;
    this._subject = subject;
    this._message = message;
    Object.freeze(this);
  }

  get id(){
    return this._id;
  }

  get from(){
    return this._from;
  }

  get to(){
    return this._to;
  }

  get subject() {
    return this._subject;
  }

  get message() {
    return this._message;
  }
}