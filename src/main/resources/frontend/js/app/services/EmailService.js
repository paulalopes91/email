class EmailService {

  obterEmail(){
    return new Promise((resolve, reject) => {
      let xhr = new XMLHttpRequest();
      xhr.open("GET", "http://localhost:8080/emailService/v1/email");
      xhr.setRequestHeader("Authorization", "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImV4cCI6MTQ5NTkwMzYzNX0.xXlFzoNjK5DrZR0RdogN5k-umVOoR3Ax2fdWYxc3vnUpjO9JD_oCG756AoA-r8vCILDJVZwHMBwQFSC3TniFlg");
      xhr.onreadystatechange = () => {
        if(xhr.readyState == 4){
          if(xhr.status == 200) {
            resolve(JSON.parse(xhr.responseText).map(email => new Email(email.id, email.from, email.to, email.subject, email.message)));
          }else {
            console.log(xhr.responseText);
            reject("Falha ao importar emails");
          }
        }
      };
      xhr.send();
    });
  }

  obterEmailId(id){
    return new Promise((resolve, reject) => {
      let xhr = new XMLHttpRequest();
      xhr.open("GET", "http://localhost:8080/emailService/v1/email/" + id);
      xhr.setRequestHeader("Authorization", "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImV4cCI6MTQ5NTkwMzYzNX0.xXlFzoNjK5DrZR0RdogN5k-umVOoR3Ax2fdWYxc3vnUpjO9JD_oCG756AoA-r8vCILDJVZwHMBwQFSC3TniFlg");
      xhr.onreadystatechange = () => {
        if(xhr.readyState == 4){
          if(xhr.status == 200) {
            resolve(JSON.parse(xhr.responseText).map(email => new Email(email.id, email.from, email.to, email.subject, email.message)));
          }else {
            console.log(xhr.responseText);
            reject("Falha ao importar emails");
          }
        }
      };
      xhr.send();
    });
  }

  enviarEmail(cb, email){
    let xhr = new XMLHttpRequest();
    xhr.open("POST", "http://localhost:8080/emailService/v1/email", true);
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.setRequestHeader("Authorization", "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImV4cCI6MTQ5NTkwMzYzNX0.xXlFzoNjK5DrZR0RdogN5k-umVOoR3Ax2fdWYxc3vnUpjO9JD_oCG756AoA-r8vCILDJVZwHMBwQFSC3TniFlg");
    xhr.onreadystatechange = () => {
      if(xhr.readyState == 4) {
        if(xhr.status == 201) {
          cb(null, "Email enviada com sucesso!");
        }else {
          console.log(xhr.responseText);
          cb("Erro ao enviar Email!", null);
        }
      }
    }

    let _email = {
      from: email.from,
      to: email.to,
      subject: email.subject,
      message: email.message
    }

    var emailJson = JSON.stringify(_email);
    xhr.send(emailJson);
  }

  atualizarEmail(cb, email){
    let xhr = new XMLHttpRequest();
    xhr.open("PUT", "http://localhost:8080/emailService/v1/email", true);
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.setRequestHeader("Authorization", "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImV4cCI6MTQ5NTkwMzYzNX0.xXlFzoNjK5DrZR0RdogN5k-umVOoR3Ax2fdWYxc3vnUpjO9JD_oCG756AoA-r8vCILDJVZwHMBwQFSC3TniFlg");
    xhr.onreadystatechange = () => {
      if(xhr.readyState == 4) {
        if(xhr.status == 204) {
          cb(null, "Email atualizado com sucesso!");
        }else {
          console.log(xhr.responseText);
          cb("Erro ao enviar Email!", null);
        }
      }
    }

    let _email = {
      id: email.id,
      from: email.from,
      to: email.to,
      subject: email.subject,
      message: email.message
    }

    var emailJson = JSON.stringify(_email);
    xhr.send(emailJson);
  }

  apagarEmail(cb, id){
    let xhr = new XMLHttpRequest();
    //xhr.open("DELETE", "http://10.2.152.65:8080/emailService/v1/email/" + id);
    xhr.open("DELETE", "http://localhost:8080/emailService/v1/email/" + id);
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.setRequestHeader("Authorization", "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImV4cCI6MTQ5NTkwMzYzNX0.xXlFzoNjK5DrZR0RdogN5k-umVOoR3Ax2fdWYxc3vnUpjO9JD_oCG756AoA-r8vCILDJVZwHMBwQFSC3TniFlg");
    xhr.onreadystatechange = () => {
      if(xhr.readyState == 4) {
        if(xhr.status == 204) {
          cb(null, "Email deletado com sucesso!");
        }else {
          console.log(xhr.responseText);
          cb("Erro ao deletar Email!", null);
        }
      }
    }

    xhr.send();
  }
}