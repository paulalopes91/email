$(document).ready(function() {
    $("#divGetId").hide();
    $("#divPost").hide();
    $("#divPut").hide();
    $("#divDelete").hide();
    $("#divGet").hide();

    $('#method').on('change',function(){
      $("#divGetId").hide();
      $("#divPost").hide();
      $("#divPut").hide();
      $("#divDelete").hide();
      $("#divGet").hide();
      var selection = $(this).val();
      switch(selection){
        case "get":
          $("#divGet").show();
          break;
        case "getId":
          $("#divGetId").show();
          break;
        case "post":
          $("#divPost").show();
          break;
        case "put":
          $("#divPut").show();
          break;
        case "delete":
          $("#divDelete").show();
          break;
        default:
          break;
        }
    });
});

