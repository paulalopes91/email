CREATE DATABASE email;

CREATE TABLE `email`.`email` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `remetente` VARCHAR(255) NULL,
  `destinatario` VARCHAR(255) NULL,
  `mensagem` VARCHAR(500) NULL,
  `subject` VARCHAR(50) NULL,
  PRIMARY KEY (`id`));
