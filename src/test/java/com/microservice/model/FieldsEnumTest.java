package com.microservice.model;

public enum FieldsEnumTest {
    FROM("from"),
    TO("to"),
    MESSAGE("message"),
    SUBJECT("subject");

    private String field;

    FieldsEnumTest(String field){
        this.field = field;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }
}
