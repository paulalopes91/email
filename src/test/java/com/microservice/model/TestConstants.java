package com.microservice.model;


public enum TestConstants {
    VALID_EMAIL("testmail@gmail.com"),
    INVALID_EMAIL("invalidemail"),
    VALID_MESSAGE("Valid Message!"),
    INVALID_MESSAGE("Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!Invalid Message!"),
    VALID_SUBJECT("Assunto Valido!"),
    INVALID_SUBJECT("Assunto Invalido!Assunto Invalido!Assunto Invalido!Assunto Invalido!Assunto Invalido!");

    private String message;

    TestConstants(String message){
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
