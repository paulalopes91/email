package com.microservice.model;

import com.microservice.exception.BadRequestException;
import com.microservice.validation.RestrictionType;
import org.junit.Assert;

import java.util.ArrayList;
import java.util.List;

public class ValidationTest {

    public ValidationErrorEntity getErrorForField(String fieldName, BadRequestException e){
        List<ValidationErrorEntity> fieldList = new ArrayList<ValidationErrorEntity>();
        ValidationErrorEntity error = null;
        for(ValidationErrorEntity ei : e.getErrors()){
            if(ei.field.equals(fieldName)){
                return  ei;
            }
        }
        return error;
    }

    public void assertProperty(String field, String message, RestrictionType type, BadRequestException e) {
        ValidationErrorEntity error = getErrorForField(field, e);
        Assert.assertEquals(field, error.field);
        Assert.assertEquals(message, error.message);
        Assert.assertEquals(type, error.type);
    }

}
