package com.microservice.tests;

import com.microservice.exception.BadRequestException;
import com.microservice.model.Email;
import com.microservice.model.FieldsEnumTest;
import com.microservice.model.ValidationTest;
import com.microservice.validation.EmailValidator;
import com.microservice.validation.ErrorMessage;
import com.microservice.validation.RestrictionType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static com.microservice.model.TestConstants.*;

@RunWith( SpringJUnit4ClassRunner.class )
public class EmailValidation extends ValidationTest {

    private EmailValidator validator;

    @Before
    public void setUp(){
        validator = new EmailValidator();
    }

    @Test(expected = BadRequestException.class)
    public void invalid_from_email_causes_error() throws BadRequestException {
        try{
            validator.validate(new Email(VALID_EMAIL.getMessage(), VALID_MESSAGE.getMessage(), VALID_SUBJECT.getMessage()));
        }catch (BadRequestException e){
            assertProperty(FieldsEnumTest.FROM.getField(), ErrorMessage.INVALID_EMAIL.getMessage(), RestrictionType.EMAIL_FORMAT, e);
            throw e;
        }
    }

    @Test(expected = BadRequestException.class)
    public void invalid_to_email_causes_error() throws BadRequestException {
        try{
            validator.validate(new Email(INVALID_EMAIL.getMessage(), VALID_MESSAGE.getMessage(), VALID_SUBJECT.getMessage()));
        }catch (BadRequestException e){
            assertProperty(FieldsEnumTest.TO.getField(), ErrorMessage.INVALID_EMAIL.getMessage(), RestrictionType.EMAIL_FORMAT, e);
            throw e;
        }
    }

    @Test(expected = BadRequestException.class)
    public void message_longer_than_500_chars_causes_error() throws BadRequestException {
        try{
            validator.validate(new Email(VALID_EMAIL.getMessage(), INVALID_MESSAGE.getMessage(), VALID_SUBJECT.getMessage()));
        }catch (BadRequestException e){
            assertProperty(FieldsEnumTest.MESSAGE.getField(), ErrorMessage.MESSAGE_LENGTH.getMessage(), RestrictionType.BUSINESS, e);
            throw e;
        }
    }

    @Test(expected = BadRequestException.class)
    public void subject_longer_than_50_chars_causes_error() throws BadRequestException {
        try{
            validator.validate(new Email(VALID_EMAIL.getMessage(), VALID_MESSAGE.getMessage(), INVALID_SUBJECT.getMessage()));
        }catch (BadRequestException e){
            assertProperty(FieldsEnumTest.SUBJECT.getField(), ErrorMessage.MESSAGE_LENGTH.getMessage(), RestrictionType.BUSINESS, e);
            throw e;
        }
    }
}
